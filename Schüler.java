/**@david7867z 
 *  @19.11.2018
 */
public class Schüler
{
 // Bezugsobjekte
 //Schülername
 String Vorname, Nachname;
 //Schülernoten
 int Klausur1, Klausur2, KlausurGesamt;
 int SoMi1, SoMi2, SoMiGesamt;
 int ZensurGesamt;
 //SchülerFehlstunden
 int Fehlstunden, UnentschuldigteFehlstunden;
 //Arrays
 int[] Klausuren = new int[2];
 int[] SoMi = new int[2];
 int[] Fehlstundenspeicher = new int[3];
 // Konstruktor
 /**
  * Setzt den Vor- und Nachnamen des Schülers/ der Schülerin. 
  */
 public Schüler(String pVorname, String pNachname){
  Vorname = pVorname;
  Nachname = pNachname;
 }
 public String gibName(){
  return Nachname;  
 }
 // Dienste
 /** 
  * Setzt die Klausurnote für das Erste, bzw zweite Quartal (Wenn nicht schon eine gesetzt wurde.).
  * Dies ist abhängig, ob der pQuartal 0 (erstes Quartal) oder
  * 1(zweites Quartal) ist.
  */
 public void setzeKlausurNote(int pNote, int pQuartal){
  if(pQuartal == 0){
   Klausuren[0] = pNote;
   Klausur1 = pNote;
   }else{
   Klausuren[1] = pNote;
   Klausur2 = pNote;
  }
 }
 /** 
  * Setzt die SoMinote für das Erste, bzw zweite Quartal (Wenn nicht schon eine gesetzt wurde.).
  * Dies ist abhängig, ob der pQuartal 0 (erstes Quartal) oder
  * 1(zweites Quartal) ist.
  */
  public void setzeSoMiNote(int pNote, int pQuartal){
  if(pQuartal == 0){
   SoMi[0] = pNote;
   SoMi1 = pNote;
  }else{
   SoMi[1] = pNote;
   SoMi2 = pNote;
  }
 }
 /**
  * Berechnet die Unentschuldigten Fehlstunden und speichert die Fehlstunden. 
  */
 public void fehlstunden(int pFehlstunden, int pEntschuldigteFehlstunden){
  int ergebnis;
  Fehlstundenspeicher[1] = pFehlstunden;
  UnentschuldigteFehlstunden = Fehlstundenspeicher[1] - pEntschuldigteFehlstunden;
 }
 /**
  * Berechnet den Mittelwert aller Noten(0).
  * Berechnet den Mittelwert der Klausurnoten(1).
  * Berechnet den Mittelwert der SoMi-Noten(2).
  */
 public void rechner(){
  int KlausurTemp = 0, SoMiTemp = 0; 
  KlausurTemp = Klausuren[0] + Klausuren[1];
  SoMiTemp = SoMi[0] + SoMi[1];
  KlausurGesamt = KlausurTemp / 2;
  SoMiGesamt = SoMiTemp / 2;
  ZensurGesamt = (KlausurGesamt + SoMiGesamt) / 2;
 }
}










 /*
  * Berechnet den Mittelwert aller Noten(0).
  * Berechnet den Mittelwert der Klausurnoten(1).
  * Berechnet den Mittelwert der SoMi-Noten(2).
  
 /*public int alterRechner1(int pTyp){  
  int ergebnis = 0, ergebnis1 = 0; 
  if(pTyp == 0){
   if (Klausuren[2] != 0 && SoMi[2] != 0){
    ergebnis1 = KlausurGesamt + SoMiGesamt;
    ergebnis = ergebnis1 / 2;
   }else{
    ergebnis1 = alterRechner(1) + alterRechner(2);   
    ergebnis = ergebnis1 / 2;
    ergebnis = ZensurGesamt;
   }
  }else if(pTyp == 1){
   KlausurGesamt = Klausuren[0] + Klausuren[1];  
   ergebnis = KlausurGesamt / 2;
   KlausurGesamt = ergebnis;
  }else if(pTyp == 2){
   SoMiGesamt = SoMi[0] + SoMi[1];  
   ergebnis = SoMiGesamt / 2; 
   SoMiGesamt = ergebnis;
  }
  return ergebnis;
 } */ 

  /*
  * Berechnet den Mittelwert aller Noten(0).
  * Berechnet den Mittelwert der Klausurnoten(1).
  * Berechnet den Mittelwert der SoMi-Noten(2).
  *
 public int alterRechner2(int pNumber){
  int a = 0, b = 0, result = 0, result1 = 0;
  if(pNumber == 0){
   if(Klausuren[2] != 0 || SoMi[2] != 0){
    a = Klausuren[2];
    b = SoMi[2];
   }else{
    a = Rechner(1);
    b = Rechner(2);
   }
  }else if(pNumber == 1){
   a = Klausuren[0];
   b = Klausuren[1];
  }else if(pNumber == 2){
   a = SoMi[0];
   b = SoMi[1];
  }
  result1 = a + b;
  result = result1 / 2;
  return result; 
 }
*/