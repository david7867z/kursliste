public class Node<ContentType> {
 private ContentType content;
 private Node<ContentType> next; 
 /**
  * erzeugt einen Node
  * @param pContent das Objekt vom Typ ContentType, das in dem Node gespeichert werden soll.
  */
 public Node(ContentType pContent){
  content = pContent;
 }
 /**
  * gibt das gespeicherte Objekt zurueck
  * @return
  */
 public ContentType getContent() {
  return content;
 }  
 /**
  * veraendert das gespeicherte Objekt.
  * @param pContent
  */
 public void setContent(ContentType pContent) {
  content = pContent;
 }  
 /**
  * gibt einen Verweis auf den naechsten Knoten zurueck.
  */
 public Node<ContentType> getNext() {
  return next;
 }
 /**
  * setzt einen Verweis auf den naechsten Knoten.
  * @param pNext
  */
 public void setNext(Node<ContentType> pNext) {
  next = pNext;
 }
}